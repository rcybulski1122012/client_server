class ValidationError(Exception):
    def get_response(self):
        return {"error": self.error_message}

    @property
    def error_message(self):
        try:
            return self.args[0]
        except IndexError:
            return "Invalid or missing arguments"
