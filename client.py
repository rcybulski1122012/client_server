import json
import pprint
import socket


class CommandClient:
    def __init__(self, host="127.0.0.1", port=65432, encoding="utf-8"):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((host, port))
        self.encoding = encoding

    def run(self):
        while True:
            request = bytes(input("Request: "), encoding=self.encoding)
            self.s.sendall(request)
            data = self.s.recv(1024)
            response = json.loads(data.decode(encoding=self.encoding))
            print("Server response: ")
            pprint.pprint(response)

            if "stop" in response.keys():
                break


if __name__ == "__main__":
    client = CommandClient()

    client.run()
