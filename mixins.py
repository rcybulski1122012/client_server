from exceptions import ValidationError


class NoArgumentsMixin:
    def validate_arguments(self, args):
        if args != {}:
            raise ValidationError("The command does not require any arguments")
