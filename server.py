import json
import socket

from commands import HelpCommand, InfoCommand, StopCommand, UpTimeCommand
from exceptions import ValidationError
from utils import parse_request


class CommandServer:
    VERSION = "2.0.0"

    def __init__(self, host="127.0.0.1", port=65432, encoding="utf-8"):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind((host, port))
        self.encoding = encoding
        self.commands = {}
        self.running = False

    def __del__(self):
        self.s.close()

    def register_command(self, command_str, command_cls):
        self.commands[command_str] = command_cls(server=self)

    def run(self):
        self.running = True
        self.s.listen()
        while self.running:
            conn, addr = self.s.accept()
            with conn:
                print(f"Connected by {addr}")
                while True:
                    data = conn.recv(1024)
                    if not data:
                        break
                    response = self.handle(data.decode(encoding=self.encoding))
                    conn.sendall(bytes(response, encoding=self.encoding))

    def handle(self, request):
        command_str, args = parse_request(request)

        try:
            command = self.commands[command_str]
            command.validate_arguments(args)
            response = command.get_response()
        except ValidationError as e:
            response = e.get_response()
        except KeyError:
            response = {"error": "Command does not exist"}

        return json.dumps(response)


if __name__ == "__main__":
    server = CommandServer()
    server.register_command("uptime", UpTimeCommand)
    server.register_command("info", InfoCommand)
    server.register_command("help", HelpCommand)
    server.register_command("stop", StopCommand)

    server.run()
