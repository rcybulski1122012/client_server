from abc import ABC, abstractmethod
from datetime import datetime

from mixins import NoArgumentsMixin


class BaseCommand(ABC):
    help = ""

    def __init__(self, server):
        self.server = server

    def validate_arguments(self, args):
        """
        Should raise ValidationError if argument are not valid
        """
        pass

    @abstractmethod
    def get_response(self, args=None):
        pass


class UpTimeCommand(NoArgumentsMixin, BaseCommand):
    help = "Returns server life time"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.start = datetime.now()

    def get_response(self, args=None):
        stop = datetime.now()
        live_time = stop - self.start

        return {"uptime": str(live_time)}


class InfoCommand(NoArgumentsMixin, BaseCommand):
    help = "Returns server version and date of creation"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.creation_date = datetime.today().date()

    def get_response(self, args=None):
        return {
            "version": self.server.VERSION,
            "creation_date": str(self.creation_date),
        }


class HelpCommand(NoArgumentsMixin, BaseCommand):
    help = "Returns list of commands with short description"

    def get_response(self, args=None):
        return {
            "commands": [
                {"command": command_str, "description": command.help}
                for command_str, command in self.server.commands.items()
            ]
        }


class StopCommand(NoArgumentsMixin, BaseCommand):
    help = "Stops the server and the client"

    def get_response(self, args=None):
        self.server.running = False

        return {"stop": "stop"}
