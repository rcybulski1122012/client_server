from exceptions import ValidationError


def parse_request(request):
    """
    Request should look like this:
    command?arg1=val1&arg2=val2&arg3=val3...
    So ?, & and = are forbidden in args/commands names
    Whitespaces are permitted
    """

    try:
        command_str, args_str = request.split("?")
    except ValueError:
        if "?" in request:
            raise ValidationError("The request is invalid")
        return request, {}
    command_str = command_str.strip()

    args = {}
    for arg_str in args_str.strip().split("&"):
        try:
            name, value = arg_str.split("=")
        except ValueError:
            raise ValidationError("The request in invalid")
        value, name = value.strip(), name.strip()
        if value.isnumeric():
            value = int(value)

        args[name] = value

    return command_str, args
